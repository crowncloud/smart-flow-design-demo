import Vue from 'vue'
import App from './App.vue'

import router from './router/';
import VueStorage from 'vue-ls';
import "./core/components_use";

import Crownboot from 'crownboot-antd-vue/lib/crownboot-antdv-mini.umd';
import 'crownboot-antd-vue/lib/crownboot-antdv-mini.css';

import 'smart-flow-design/lib/smart-flow-design-mini.umd.min.1'
import 'smart-flow-design/lib/smart-flow-design-mini.umd.min.2'
import 'smart-flow-design/lib/smart-flow-design-mini.umd.min.3'
import 'smart-flow-design/lib/smart-flow-design-mini.umd.min.4'
import 'smart-flow-design/lib/smart-flow-design-mini.umd.min.5'
import SmartFlowDesign from 'smart-flow-design/lib/smart-flow-design-mini.umd.min'
import 'smart-flow-design/lib/smart-flow-design.css'

import store from './store';
import { VueAxios } from './utils/request';
import UUID from 'vue-uuid';

Vue.use(VueStorage, {
  namespace: 'pro__', // key prefix
  name: 'ls', // name variable Vue.[ls] or this.[$ls],
  storage: 'local', // storage name session, local, memory
});
Vue.prototype.$store = store;
Vue.use(VueAxios);
Vue.use(UUID);
Vue.use(Crownboot);
Vue.use(SmartFlowDesign);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
