import Vue from 'vue';
import Vuex from 'vuex';
import {flowStore} from 'smart-flow-design/lib/smart-flow-design-mini.umd.min'
Vue.use(Vuex);
const store = new Vuex.Store({
  modules: {
     flow:flowStore,
  },
});

export default store;
